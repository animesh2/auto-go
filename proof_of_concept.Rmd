/---
title: "Proof of concept autogo"
author: "Eleonora Sperandio"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    df_print: paged

---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = F, error = TRUE)
knitr::opts_knit$set(root.dir = "/auto-go")
```

```{r, include=FALSE}
suppressPackageStartupMessages({
  library(tidyverse)
  library(DESeq2)
  library(enrichR)
  library(xlsx)
  library(circlize)
  library(ComplexHeatmap)
  })
```

The data are downloaded by the R package TCGAbiolinks. For completeness, the procedure is shown below, but it is not necessary to perform it as the necessary files are already saved in the test_data_TCGA/TUMOR folder. If you want to run it again, change the variable "already_saved" to F.

# Variables to set
```{r}
already_saved <- T

project <- "TCGA-SKCM"
```


# Counts
```{r}
if (!already_saved) {
  library(TCGAbiolinks)
  
  query <- GDCquery(project = project,
                    data.category = "Transcriptome Profiling",
                    data.type = "Gene Expression Quantification", 
                    workflow.type = "HTSeq - Counts", legacy = F)
  # samples <- getResults(query,cols = c("cases"))
  # dataSmTP <- TCGAquery_SampleTypes(barcode = samples, typesample = "TP")
  # query <- GDCquery(project = project,
  #                   data.category = "Transcriptome Profiling",
  #                   data.type = "Gene Expression Quantification", 
  #                   workflow.type = "HTSeq - Counts", legacy = F,
  #                   barcode = dataSmTP)
  GDCdownload(query = query)
  data <- GDCprepare(query)
  counts <- SummarizedExperiment::assay(data)
  
  conversion <-  read_delim("data/conversion_ensembl_hgnc.txt", delim = "\t", col_types = cols())
  
  counts <- data.frame(counts) %>% 
    rownames_to_column(var = "gene") %>% 
    inner_join(conversion, by=c("gene"="ensembl_gene_id")) %>% 
    dplyr::select(-gene) %>% 
    dplyr::filter(!duplicated(external_gene_name)) %>%
    column_to_rownames(var = "external_gene_name") %>% 
    rename_with(~ gsub("\\.", "-", .x))
  
  colnames(counts) <- str_extract(colnames(counts), ".{12}")
  counts <- counts[, !duplicated(colnames(counts))]
  counts <- counts %>% 
    rownames_to_column(var = "gene_id")
  
  if (!dir.exists(paste0("data/test_data/TCGA/",gsub("TCGA-","",project)))) dir.create(paste0("data/test_data/TCGA/",gsub("TCGA-","",project)))
  write.table(counts, paste0("data/test_data/TCGA/",gsub("TCGA-","",project),"/",project,"_counts_hgnc.txt"), quote = F, sep = '\t', row.names = F, col.names = T)
}
```

# Loading already saved counts
```{r, warning=FALSE}
getwd()
counts <- read_tsv(paste0("data/test_data/",project,"/",project,"_counts_hgnc.txt"), col_types = cols())
```

# Clinical data
```{r}
if (!already_saved & project == "TCGA-KIRC") {
  query <- GDCquery(project = project, 
                    data.category = "Clinical",
                    data.type = "Clinical Supplement", 
                    data.format = "BCR Biotab",
                    file.type = "patient")
  GDCdownload(query)
  clinical <- GDCprepare(query)
  
  clinical <- as.data.frame(clinical) %>%
    rename_all(~gsub(paste0("clinical_patient_",tolower(gsub("TCGA-","",project)),"\\."),"",.x)) %>%
    dplyr::slice(-c(1L:2L)) %>% 
    dplyr::select(bcr_patient_barcode,) %>%
    filter(bcr_patient_barcode %in% colnames(counts)[-1]) %>%
    # this is specific for KIRC patients
    { if (project == "TCGA-KIRC") mutate(.,group = case_when(
      age_at_initial_pathologic_diagnosis < 50 ~ "less50",
      age_at_initial_pathologic_diagnosis >= 50 & age_at_initial_pathologic_diagnosis < 58 ~ "50_57",
      age_at_initial_pathologic_diagnosis >= 58 & age_at_initial_pathologic_diagnosis < 64 ~ "58_63",
      age_at_initial_pathologic_diagnosis >= 64 & age_at_initial_pathologic_diagnosis < 73 ~ "64_72",
      age_at_initial_pathologic_diagnosis >= 73 ~ "greater72"
    )) %>%
        dplyr::select(-age_at_initial_pathologic_diagnosis) %>%
        dplyr::rename(sample=bcr_patient_barcode) else . }
  
  write.table(clinical, paste0("data/test_data/TCGA/",gsub("TCGA-","",project),"/groups.txt"), row.names = F, quote = F, sep = "\t")
  
  comparisons <- as.data.frame(t(combn(unique(clinical$group), 2))) %>%
    dplyr::rename(treatment=V1, control=V2)
  
  write.table(comparisons, paste0("data/test_data/TCGA/",gsub("TCGA-","",project),"/comparisons.txt"), row.names = F, quote = F, sep = "\t")
}

if (!already_saved & project == "TCGA-SKCM") {
  library(cgdsr)
  mycgds <- CGDS("http://www.cbioportal.org/")
  cl <- getCaseLists(mycgds, "skcm_tcga_pan_can_atlas_2018")
  list_id_rna <- cl[ grep ("RNA Seq V2", cl$case_list_name), ]$case_list_id[1]
  clinical <- getClinicalData(mycgds, list_id_rna) %>% 
    rownames_to_column(var="PAT_ID") %>% 
    mutate(PAT_ID = gsub("\\.","-",PAT_ID),
           PAT_ID = gsub("-[0-9]+$", "", PAT_ID)) %>% 
    select(PAT_ID, contains("TMB")) %>% 
    filter(PAT_ID %in% colnames(counts) & !is.na(TMB_NONSYNONYMOUS) & !duplicated(PAT_ID)) %>% 
    mutate(Q = paste0("TMBQ",ntile(TMB_NONSYNONYMOUS, 4)))
  
  counts <- counts %>% 
    column_to_rownames(var = "gene_id") %>% 
    select(all_of(clinical$PAT_ID)) %>% 
    rownames_to_column(var = "gene_id")

  #write.table(counts, paste0("test_data_TCGA/",gsub("TCGA-","",project),"/",project,"_counts_hgnc.txt"), quote = F, sep = '\t', row.names = F, col.names = T)
  
  groups <- clinical %>% 
    select(-TMB_NONSYNONYMOUS) %>% 
    dplyr::rename(sample = PAT_ID, group = Q)
  
  
  comparisons <- t(combn(unique(groups$group), 2)) %>% 
    as.data.frame() %>% 
    dplyr::rename(treatment = V1, control = V2)

  write.table(clinical, paste0("data/test_data/TCGA/",gsub("TCGA-","",project),"/groups.txt"), row.names = F, quote = F, sep = "\t")
  write.table(comparisons, paste0("data/test_data/TCGA/",gsub("TCGA-","",project),"/comparisons.txt"), row.names = F, quote = F, sep = "\t")

}
```

# Loading already saved groups and comparisons
```{r}
groups <- read.table(paste0("data/test_data/",project,"/groups.txt"), sep = '\t', header = T)
comparisons <- read.table(paste0("data/test_data/",project,"/comparisons.txt"), sep = '\t', header = T)
```


###### USING AUTOGO PACKAGE
```{r}
files <- list.files(path = "R", recursive = T, all.files = T, full.names = T)
invisible(sapply(files, source))
```


Step 1: Differential expression analysis by the "deseq_analysis.R" function.
```{r}
deseq_analysis(counts = counts,
               groups = groups,
               comparisons = comparisons,
               padj_threshold = 0.05,
               log2FC_threshold = 2, 
               pre_filtering = T,
               save_excel = F,
               where_results = "results/",
               outfolder = "test_TCGA-SKCM/")
```

Step 2: Volcano Plot
```{r}
path_res <- "results/test_TCGA-SKCM"
all_path_res <- list.files(path = path_res, pattern = "_allres.tsv", recursive = T, full.names = T)
res_lists <- lapply(all_path_res, function (x) read_tsv(x, col_types = cols()))
names(res_lists) <- gsub("results/test_TCGA-SKCM/|/DE_.*", "",all_path_res)

invisible(lapply(names(res_lists), function (i) volcanoplot(res_lists[[i]], my_comparison = i, log2FC_thresh = 2 , padj_thresh = 0.05, where_results = "results/", outfolder = "test_TCGA-SKCM/")))
```

Step 3: Reading gene lists
```{r}
gene_list <- read_gene_list(where_results = "results/", outfolder = "test_TCGA-SKCM/", log2FC_threshold=2, which_list = "everything")
```

Step 4: Enrichment Analysis
```{r}
lapply(names(gene_list), function (i) autoGO(gene_list[[i]], my_comparison = i, where_results = "results/", outfolder = "test_TCGA-SKCM/"))
```

Step 5: Read Enrich Tables
```{r}
enrich_list <- read_enrich_tables(where_results = "results/", outfolder = "test_TCGA-SKCM/",log2FC_threshold = 2, padj_threshold = 0.05, which_list = "everything")
```

Step 6: Barplot and Lolliplot
```{r}
invisible(lapply(names(enrich_list), function (i) barplotGO(enrich_list[[i]], my_comparison = i, where_results = "results/", outfolder = "test_TCGA-SKCM/")))
invisible(lapply(names(enrich_list), function (i) lolliGO(enrich_list[[i]], my_comparison = i, where_results = "results/", outfolder = "test_TCGA-SKCM/")))
```

Step 7: HeatmapGO
```{r}
dbs <- c("GO_Molecular_Function_2021", "GO_Cellular_Component_2021", "GO_Biological_Process_2021", "KEGG_2021_Human")


invisible(lapply(dbs, function (i) heatmapGO(lib = i, pval_filter = 0.05, log2FC_threshold = 2, where_results = "results/", outfolder = "test_TCGA-SKCM/", which_list = "up_genes")))
invisible(lapply(dbs, function (i) heatmapGO(lib = i, pval_filter = 0.05, log2FC_threshold = 2, where_results = "results/", outfolder = "test_TCGA-SKCM/", which_list = "down_genes")))
invisible(lapply(dbs, function (i) heatmapGO(lib = i, pval_filter = 0.05, log2FC_threshold = 2, where_results = "results/", outfolder = "test_TCGA-SKCM/", which_list = "up_down_genes")))
```

