# Auto-GO: Reproducible, Robust and High Quality Ontology Enrichment Visualizations

Auto-GO is a framework that enables automated, high quality Gene Ontology enrichment analyses visualizations. It also features a handy wrapper for Differential Expression analysis around the DESeq2 package (Love et. al 2014) and a handy wrapper around the single-sample implementation of the GSEA algorithm (ssGSEA), useful for low-numerosity transcriptional profiles. 

The whole framework is structured in different, independent functions, in order to let the user decide which steps of the analysis perform and which plot to produce.

## Requirements
* R version 4.1 or higher.

    It will need several packages to run:
    * dplyr
    * tidyverse
    * readr
    * gdata
    * reshape2
    * tidyr
    * stringr
    * xlsx
    * circlize
    * DESeq2
    * ComplexHeatmap
    * enrichR
    * gsva
    

## Install

```
git clone https://gitlab.com/bioinfo-ire-release/auto-go
```
    
## Usage

For a thorough step-by-step explanation, please see the [tutorial](tutorial.html). 

The repository also contains a Dockerfile to directly test one TCGA use case for multiple DE and GO enrichment visualizations. You can fetch it from the Docker hub via 

```
docker pull mpallocc/auto-go:latest
```

or you can compile it from the repo

```
docker build -t auto-go . 
```

The image can be launched via: 

```
docker run -it auto-go:latest bash 
```

While inside the docker image, just run: 
 
```
Rscript -e "rmarkdown::render('proof_of_concept.Rmd', params=(already_saved=F))"
```

To produce the HTML compiled file of the TCGA-SKCM multi-DE comparisons. 
    
## References
The first implementation of the logical framework is implemented via Enrichr APIs and DESeq2:

Chen EY, Tan CM, Kou Y, Duan Q, Wang Z, Meirelles GV, Clark NR, Ma'ayan A. Enrichr: interactive and collaborative HTML5 gene list enrichment analysis tool. BMC Bioinformatics. 2013;128(14)

Kuleshov MV, Jones MR, Rouillard AD, Fernandez NF, Duan Q, Wang Z, Koplev S, Jenkins SL, Jagodnik KM, Lachmann A, McDermott MG, Monteiro CD, Gundersen GW, Ma'ayan A. Enrichr: a comprehensive gene set enrichment analysis web server 2016 update. Nucleic Acids Research. 2016; gkw377.

Love, M. I., Huber, W., & Anders, S. (2014). Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2. Genome Biology, 15(12).
